import { decode } from "punycode";
import config from "../config";
import { rlp } from 'ethereumjs-util'

const DEFAULT_PATH = "/global/block/head/durable/wasm_2_0_0"

async function get(path) {
    try {
        let rpc_path = config.rollupNodeRpc + DEFAULT_PATH + path;
        let response = await fetch(rpc_path, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
            mode: 'cors',
        });
        return response.json();

    } catch (error) {
        console.error(error);
    }
}

function read(key) {
    return "/value\?key=" + key;
}

function list(key) {
    return "/subkeys\?key=" + key;
}

export async function getPlayers() {
    let key = "/players";
    let path = list(key);
    return get(path);
}

function hexToBytes(hex) {
    var bytes = [];
    for (var c = 0; c < hex.length; c += 2) {
        bytes.push(parseInt(hex.substr(c, 2), 16));
    }
    return bytes;
}

function hexToString(hex) {
    let bytes = hexToBytes(hex);
    let decoder = new TextDecoder('utf-8');
    return decoder.decode(new Uint8Array(bytes));
}

export async function getPlayerName(tz1) {
    let key = read("/players/" + tz1 + "/name");
    let name_hex = await get(key);
    let name = hexToString(name_hex);
    return name;
}

export async function getPlayerBalance(tz1) {
    let key = read("/players/" + tz1 + "/balance");
    let hex = await get(key);

    if (hex == null) {
        return 0;
    } else {
        return parseInt(hex, 16)
    }
}

export async function getLastGetPoketezDay(tz1) {
    let key = read("/players/" + tz1 + "/last_poketez");
    let hex = await get(key);
    if (hex == null) {
        return 0;
    } else {
        return parseInt(hex, 16);
    }
}

export async function getLastActionResults(tz1) {
    let key = read("/players/" + tz1 + "/last_action_results");
    let hex = await get(key);
    if (hex == null) {
        return 0;
    } else {
        return parseInt(hex, 16);
    }
}

function decodePokedex(bytes) {
    let result = []
    for (let i = 0; i < 151 * 3; i += 3) {
        const number = bytes[i];
        const quantity = (bytes[i + 1] << 8 | bytes[i + 2]);
        result.push({ number, quantity })
    }
    return result;
}

export async function getPokedex(tz1) {
    let key = read("/players/" + tz1 + "/pokedex");
    let hex = await get(key);
    if (hex == null) {
        return null
    } else {
        return decodePokedex(hexToBytes(hex));
    }
}

export function bytesToHex(bytes) {
    return Array.from(bytes, byte => byte.toString(16).padStart(2, '0')).join('');
}

function decodeInventoryNoSort(bytes) {
    let n = bytes.length / 10;
    let inventory = [];
    for (let i = 0; i < bytes.length; i += 10) {
        const id = bytes[i];
        var u8Array = new Uint8Array([bytes[i + 1], bytes[i + 2], bytes[i + 3], bytes[i + 4]]);
        var view = new DataView(u8Array.buffer);
        const uuid = view.getInt32();
        const rarity = bytes[i + 5];
        const stat_rarity = (bytes[i + 6] << 8 | bytes[i + 7]);
        const stat_cards = (bytes[i + 8] << 8 | bytes[i + 9]);
        inventory.push({ id, uuid, rarity, stat_rarity, stat_cards })
    }
    return inventory;
}

function decodeInventory(bytes) {
    let inventory = decodeInventoryNoSort(bytes);
    let sortedInventory = inventory.slice().sort((a, b) => a.id - b.id);
    return sortedInventory;
}

export async function getInventory(tz1) {
    let key = read("/players/" + tz1 + "/inventory");
    let hex = await get(key);
    if (!hex) {
        return [];
    }

    let bytes = hexToBytes(hex);
    return decodeInventory(bytes);
}

export async function getResults(tz1) {
    let key = read("/players/" + tz1 + "/action_results");
    let hex = await get(key);

    if (!hex) {
        return [];
    }

    const rlpBytes = Buffer.from(hex, 'hex');
    const decoded = rlp.decode(rlpBytes);

    const explorations =
        decoded.map((item) => {
            return decodeInventoryNoSort(item)
        });
    return explorations;
}

export async function getBonus(tz1) {
    let key = read("/players/" + tz1 + "/bonus");
    let hex = await get(key);

    if (!hex) {
        return null;
    }

    let bytes = hexToBytes(hex);

    const id = bytes[0];
    var u8Array = new Uint8Array([bytes[1], bytes[2], bytes[3], bytes[4]]);
    var view = new DataView(u8Array.buffer);
    const uuid = view.getInt32();
    const rarity = bytes[5];
    const stat_rarity = (bytes[6] << 8 | bytes[7]);
    const stat_cards = (bytes[8] << 8 | bytes[9]);

    return { id, uuid, rarity, stat_rarity, stat_cards }
}
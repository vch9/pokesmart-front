
import React, { Fragment, useState, useEffect } from "react";
import Players from "./components/Players";
import Player from "./components/Player";
import { TezosToolkit } from "@taquito/taquito";
import NavBar from "./components/NavBar";
import Leaderboard from "./components/Leaderboard";
import Results from "./components/Results";
import * as rpc from './utils/rpc';

const App = () => {
    const [wallet, setWallet] = useState<any>(null);
    const [Tezos, setTezos] = useState<TezosToolkit>(
        new TezosToolkit("https://ghostnet.ecadinfra.com")
    );
    const [beaconConnection, setBeaconConnection] = useState<boolean>(false);
    const [players, setPlayers] = useState(null);
    const [loading, setLoading] = useState(true);

    const { search } = window.location;
    const queryParams = new URLSearchParams(location.search);
    let name = queryParams.get('name');

    let player = window.sessionStorage.getItem('player');
    if (name) {
        player = name;
    }

    useEffect(() => {
        (async () => {
            let players = await rpc.getPlayers();
            players = await Promise.all(players.map(async tz1 => {
                let name = await rpc.getPlayerName(tz1);
                window.sessionStorage.setItem(name, tz1);
                return { name, tz1 };
            }));
            setPlayers(players);
            setLoading(false);
        })();
    }, []);

    if (loading) {
        return "Loading..";
    }

    if (!player) {
        return (<Fragment>
            <NavBar
                wallet={wallet}
                setWallet={setWallet}
                Tezos={Tezos}
                setTezos={setTezos}
                setBeaconConnection={setBeaconConnection} />
            <Players players={players} />

        </Fragment >)
    } else {
        if (player == "leaderboard") {
            return (<Fragment>
                <NavBar
                    wallet={wallet}
                    setWallet={setWallet}
                    Tezos={Tezos}
                    setTezos={setTezos}
                    setBeaconConnection={setBeaconConnection} />
                <Leaderboard players={players} />
            </Fragment>)

        } else if (player == "results") {
            return (<Fragment>
                <NavBar
                    wallet={wallet}
                    setWallet={setWallet}
                    Tezos={Tezos}
                    setTezos={setTezos}
                    setBeaconConnection={setBeaconConnection} />
                <Results players={players} />
            </Fragment>)
        } else {

            return (<Fragment>
                <NavBar
                    wallet={wallet}
                    setWallet={setWallet}
                    Tezos={Tezos}
                    setTezos={setTezos}
                    setBeaconConnection={setBeaconConnection} />
                <Player player={player} players={players} />
            </Fragment>
            )
        }
    }
}
export default App;
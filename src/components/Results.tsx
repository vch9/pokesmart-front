import { useEffect, useState } from "react";
import { showResults } from "./Player";
import * as rpc from '../utils/rpc';

function Results({ players }) {
    let [results, setResults] = useState(null);
    let [loading, setLoading] = useState(true);


    useEffect(() => {
        (async () => {
            let new_results = await Promise.all(players.map(async inputPlayer => {
                let res = await rpc.getResults(inputPlayer.tz1);
                let lastActionResult = await rpc.getLastActionResults(inputPlayer.tz1);
                let player = { tz1: inputPlayer.tz1, name: inputPlayer.name, lastActionResult };
                return { player, res };
            }))
            setResults(new_results);
            setLoading(false);
        })();
    }, []);

    if (loading) {
        return "Loading..";
    }

    let sortedResults = results.slice().sort((a, b) => b.player.lastActionResult - a.player.lastActionResult);

    return (<div>
        <br></br>
        {sortedResults.map(({ player, res }) => {
            if (res == null || res.length == 0) {
                return "";
            }
            return (
                <div>
                    <span>{player.name} ({new Date(player.lastActionResult * 1000).toLocaleDateString()}, {new Date(player.lastActionResult * 1000).toLocaleTimeString()}):</span>
                    {showResults(res, false, () => { }, false)}
                    <br></br>
                    <br></br>
                </div>
            )
        })}

    </div>)
}

export default Results;
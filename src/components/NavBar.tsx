import { useEffect, useState, Dispatch, SetStateAction } from "react";
import { connectWallet, disconnectWallet, getActiveAccount } from '../utils/wallet'
import { BeaconWallet } from "@taquito/beacon-wallet";
import {
    NetworkType,
    BeaconEvent,
    defaultEventCallbacks,
} from "@airgap/beacon-dapp"
import { TezosToolkit } from "@taquito/taquito";
import config from "../config";

interface Props {
    wallet: BeaconWallet;
    setWallet: Dispatch<SetStateAction<any>>;
    Tezos: TezosToolkit;
    setTezos: Dispatch<SetStateAction<TezosToolkit>>;
    setBeaconConnection: Dispatch<SetStateAction<boolean>>;
}

export default function NavBar({ wallet, setWallet, Tezos, setTezos,
    setBeaconConnection }) {

    const disconnect = async (): Promise<void> => {
        await disconnectWallet();
        setWallet(null);
        setBeaconConnection(false);
        const tezosTK = new TezosToolkit(config.tezosEndpoint);
        setTezos(tezosTK);
    };

    const connectWallet = async (): Promise<void> => {
        try {
            const wallet = new BeaconWallet({
                name: "PokeSmart",
                preferredNetwork: config.network,
                disableDefaultEvents: false, // Disable all events when true/ UI. This also disables the pairing alert.
            });
            Tezos.setWalletProvider(wallet);

            await wallet.requestPermissions({
                network: {
                    type: config.network,
                    rpcUrl: config.tezosEndpoint,
                },
            });
            const _pkh = await wallet.getPKH();
            setWallet(wallet);
            setBeaconConnection(true);
        } catch (error) {
            console.log(error);
        }
    };

    const goHome = () => {
        window.sessionStorage.setItem('player', '');
        window.location = window.location.pathname;
    }

    const goLeaderboard = () => {
        window.sessionStorage.setItem('player', 'leaderboard');
        window.location = window.location.pathname;
    }

    const goResults = () => {
        window.sessionStorage.setItem('player', 'results');
        window.location = window.location.pathname;
    }

    useEffect(() => {
        (async () => {
            let w = await getActiveAccount();
            setWallet(w);
        })();
    }, []);

    return (
        <nav className="bg-gray-800 h-14 flex items-center px-10 justify-between">
            <div>
                <button onClick={goHome}>Accueil</button>
                <button onClick={wallet ? disconnect : connectWallet}>{wallet ? "Déconnexion" : "Connexion"}</button>
                <button onClick={goLeaderboard}>Classement</button>
                <button onClick={goResults}>Résultats journalier</button>
            </div>
        </nav>
    );
}
